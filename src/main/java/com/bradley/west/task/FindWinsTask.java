package com.bradley.west.task;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.bradley.west.model.CurrencyValue;

@Component
@Scope("prototype")
public class FindWinsTask implements Runnable {

	private double stopLoss;
	
	private double takeProfit;
	
	private List<CurrencyValue> cvs;
	
	@Override
	public void run() {
		
		for(CurrencyValue value: cvs) {
			if(value.getOpenValue() <= stopLoss) {
				//LOGGER.info("Win {}", value.getTime());
			} 
			
			if(value.getOpenValue() >= takeProfit) {
				//LOGGER.info("Loss {}", value.getTime());
			}
		}
		
	}

	public void setStopLoss(double stopLoss) {
		this.stopLoss = stopLoss;
	}

	public void setTakeProfit(double takeProfit) {
		this.takeProfit = takeProfit;
	}

	public void setCvs(List<CurrencyValue> cvs) {
		this.cvs = cvs;
	}

	
	
}
