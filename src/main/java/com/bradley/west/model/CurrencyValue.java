package com.bradley.west.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyValue {

	@JsonProperty("Time")
	private String time;
	
	@JsonProperty("Open")
	private double openValue;
	
	@JsonProperty("High")
	private double highValue;
	
	@JsonProperty("Low")
	private double lowValue;
	
	@JsonProperty("Close")
	private double closeValue;
	
	@JsonProperty("Volume")
	private double volume;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public double getOpenValue() {
		return openValue;
	}

	public void setOpenValue(double openValue) {
		this.openValue = openValue;
	}

	public double getHighValue() {
		return highValue;
	}

	public void setHighValue(double highValue) {
		this.highValue = highValue;
	}

	public double getLowValue() {
		return lowValue;
	}

	public void setLowValue(double lowValue) {
		this.lowValue = lowValue;
	}

	public double getCloseValue() {
		return closeValue;
	}

	public void setCloseValue(double closeValue) {
		this.closeValue = closeValue;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}
	
	
}
