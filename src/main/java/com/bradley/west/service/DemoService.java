package com.bradley.west.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bradley.west.dao.ICurrencyValueDao;
import com.bradley.west.model.CurrencyValue;
import com.bradley.west.task.FindWinsTask;

@Service
public class DemoService implements IDemoService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoService.class);
	  
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	private ICurrencyValueDao currencyValueDao;
	
	@Override
	public void demoTest() {
		
		
		List<CurrencyValue> cvs = currencyValueDao.getCurrencyValues();
		
		double spread = 0.001;
		double stopLoss = 0.0001;
		double takeProfit = 0.001;
		
		Calendar start = Calendar.getInstance();
		
		for(CurrencyValue value: cvs) {
			double calcSL = value.getOpenValue() - (spread/stopLoss);
			double calcTP = value.getOpenValue() + stopLoss;
			
			FindWinsTask fwt = new FindWinsTask();
			fwt.setCvs(cvs);
			fwt.setStopLoss(calcSL);
			fwt.setTakeProfit(calcTP);
			
			taskExecutor.execute(fwt);
			
			//checkForValue(calcSL, calcTP, cvs);
			
		}
		
		for(;;) {
			int count = taskExecutor.getActiveCount();
			LOGGER.info("Active Theads: {}", count);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(count == 0){
				break;
			}
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:SS");
		
		LOGGER.info("Start {}", sdf.format(start.getTime()));
		
		LOGGER.info("End {}", sdf.format(Calendar.getInstance().getTime()));
		
	}

	
	@PostConstruct
	public void postConstruct() {
		demoTest();
	}
	
	private void checkForValue(double stopLoss, double takeProfit, List<CurrencyValue> cvs) {
		
		for(CurrencyValue value: cvs) {
			if(value.getOpenValue() <= stopLoss) {
				//LOGGER.info("Win {}", value.getTime());
			} 
			
			if(value.getOpenValue() >= takeProfit) {
				//LOGGER.info("Loss {}", value.getTime());
			}
		}
		
	}
}


