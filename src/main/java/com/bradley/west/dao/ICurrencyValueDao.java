package com.bradley.west.dao;

import java.util.List;

import com.bradley.west.model.CurrencyValue;

public interface ICurrencyValueDao {

	public List<CurrencyValue> getCurrencyValues();
}
