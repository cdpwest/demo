package com.bradley.west.dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bradley.west.model.CurrencyValue;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@Component
public class CurrencyValueDao implements ICurrencyValueDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyValueDao.class);
	
	@Override
	public List<CurrencyValue> getCurrencyValues() {
		
		ArrayList<CurrencyValue> cvs = new ArrayList();
		
//		CsvMapper mapper = new CsvMapper();
//		mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
		
		File csvFile = new File("EURUSD.CSV"); // or from String, URL etc
		
		try {
			
			Iterator<CurrencyValue> it = new CsvMapper()
	                .readerFor(CurrencyValue.class)
	                .with(CsvSchema.emptySchema().withHeader())
	                .readValues(csvFile);
			
			while(it.hasNext()) {
				
				cvs.add(it.next());
			}
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error");
		}
		
		
		return cvs;
	}

}
